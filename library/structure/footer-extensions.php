<?php
/**
 * Adds footer structures.
 *
 */

/****************************************************************************************/

add_action( 'fituet_footer', 'fituet_footer_widget_area', 10 );
/**
 * Displays the footer widgets
 */
function fituet_footer_widget_area() {
	get_sidebar( 'footer' );
}

/****************************************************************************************/

add_action( 'fituet_footer', 'fituet_open_sitegenerator_div', 20 );
/**
 * Opens the site generator div.
 */
function fituet_open_sitegenerator_div() {
	echo '<div id="site-generator">
				<div class="container">';
}

/****************************************************************************************/

add_action( 'fituet_footer', 'fituet_footer_info', 30 );
/**
 * function to show the footer info, copyright information
 */
function fituet_footer_info() {
	echo '<div class="copyright">';

	fituet_social_links();

	echo '</div><!-- .copyright -->';
}

/****************************************************************************************/

add_action( 'fituet_footer', 'fituet_close_sitegenerator_div', 35 );


/**
 * add content to the right side of footer
 */
function fituet_footer_rightinfo() {
	echo '<div class="footer-right">';
	echo get_theme_mod( 'fituet_footer_textbox' );
	echo '</div>';
}

add_action( 'fituet_footer', 'fituet_footer_rightinfo', 30 );

/**
 * Closes the site generator div.
 */
function fituet_close_sitegenerator_div() {
	echo '<div style="clear:both;"></div>
			</div><!-- .container -->
			</div><!-- #site-generator -->';
}

/****************************************************************************************/

add_action( 'fituet_footer', 'fituet_backtotop_html', 40 );
/**
 * Shows the back to top icon to go to top.
 */
function fituet_backtotop_html() {
	echo '<div class="back-to-top"><a href="#branding"></a></div>';
}