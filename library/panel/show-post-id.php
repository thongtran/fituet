<?php
/**
 * Fituet Show Post Type 'Post' IDs
 *
 * This file shows the Post Type 'Post' IDs in the all post table.
 * This file shows the post id and hence helps the users to see the respective post ids so that they can use in the slider options easily.
 *
 */

/****************************************************************************************/

add_action( 'admin_init', 'fituet_show_post_respective_id' );
/**
 * Hooking fituet_show_post_respective_id function to admin_init action hook.
 * The purpose is to show the respective post id in all posts table.
 */
function fituet_show_post_respective_id() {
	/**
	 * CSS for the added column.
	 */
	add_action( 'admin_head', 'fituet_post_table_column_css' );

	/**
	 * For the All Posts table
	 */
	add_filter( 'manage_posts_columns', 'fituet_column' );
	add_action( 'manage_posts_custom_column', 'fituet_value', 10, 2 );

	/**
	 * For the All Pages table
	 */
	add_filter( 'manage_pages_columns', 'fituet_column' );
	add_action( 'manage_pages_custom_column', 'fituet_value', 10, 2 );
}

/**
 * Add a new column for all posts table.
 * The column is named ID.
 */
function fituet_column( $cols ) {
	$cols['fituet-column-id'] = 'ID';

	return $cols;
}

/**
 * This function shows the ID of the respective post.
 */
function fituet_value( $column_name, $id ) {
	if ( 'fituet-column-id' == $column_name ) {
		echo $id;
	}
}

/**
 * CSS for the newly added column in all posts table.
 * The width of new column is set to 40px.
 */
function fituet_post_table_column_css() {
	?>
	<style type="text/css">
		#fituet-column-id {
			width: 40px;
		}
	</style>
	<?php
}

?>