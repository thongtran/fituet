<?php
/**
 * Controls translation of Fituet Theme.
 *
 * @since Fituet 1.3.0
 */

/**
 * Fituet is available for translation.
 * Add your files into library/languages/ directory.
 * @see http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

load_theme_textdomain( 'fituet', get_template_directory() . '/languages' );

?>