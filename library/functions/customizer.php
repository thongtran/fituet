<?php
/**
 * Add block to WordPress theme customizer
 * @package Fituet
 */
function fituet_options_register_theme_customizer( $wp_customize ) {

	$wp_customize->add_section(
		'fituet_menu_options', array(
			'title'       => __( 'Fituet Header Area', 'fituet' ),
			'description' => sprintf( __( 'Use the following settings change color for menu and website title', 'fituet' ) ),
			'priority'    => 31,
		)
	);

	$wp_customize->add_setting(
		'fituet_logo_color',
		array(
			'default'           => '#015198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_logo_color',
			array(
				'label'    => __( 'Website Title Color', 'fituet' ),
				'section'  => 'fituet_menu_options',
				'settings' => 'fituet_logo_color',
				'priority' => 1
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_logo_hover_color',
		array(
			'default'           => '#016198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_logo_hover_color',
			array(
				'label'    => __( 'Website Title Hover Color', 'fituet' ),
				'section'  => 'fituet_menu_options',
				'settings' => 'fituet_logo_hover_color',
				'priority' => 2
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_menu_color',
		array(
			'default'           => '#015198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_menu_color',
			array(
				'label'    => __( 'Menu Bar Color', 'fituet' ),
				'section'  => 'fituet_menu_options',
				'settings' => 'fituet_menu_color'
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_menu_hover_color',
		array(
			'default'           => '#016198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_menu_hover_color',
			array(
				'label'    => __( 'Menu Bar Hover Color', 'fituet' ),
				'section'  => 'fituet_menu_options',
				'settings' => 'fituet_menu_hover_color'
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_menu_item_color',
		array(
			'default'           => '#FFF',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_menu_item_color',
			array(
				'label'    => __( 'Menu Item Text Color', 'fituet' ),
				'section'  => 'fituet_menu_options',
				'settings' => 'fituet_menu_item_color',
				'priority' => 3
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_social_color',
		array(
			'default'           => '#D0D0D0',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_social_color',
			array(
				'label'    => __( 'Social Icon Color', 'fituet' ),
				'section'  => 'fituet_menu_options',
				'settings' => 'fituet_social_color',
				'priority' => 13
			)
		)
	);

	$wp_customize->add_section(
		'fituet_element_options', array(
			'title'       => __( 'Fituet Element Color', 'fituet' ),
			'description' => sprintf( __( 'Use the following settings change color for website elements', 'fituet' ) ),
			'priority'    => 32,
		)
	);

	$wp_customize->add_setting(
		'fituet_element_color',
		array(
			'default'           => '#015198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_element_color',
			array(
				'label'    => __( 'Element Color', 'fituet' ),
				'section'  => 'fituet_element_options',
				'settings' => 'fituet_element_color',
				'priority' => 4
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_element_hover_color',
		array(
			'default'           => '#016198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_element_hover_color',
			array(
				'label'    => __( 'Element Hover Color', 'fituet' ),
				'section'  => 'fituet_element_options',
				'settings' => 'fituet_element_hover_color',
				'priority' => 5
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_wrapper_color',
		array(
			'default'           => '#F8F8F8',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_wrapper_color',
			array(
				'label'    => __( 'Wrapper Color', 'fituet' ),
				'section'  => 'fituet_element_options',
				'settings' => 'fituet_wrapper_color',
				'priority' => 6
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_content_bg_color',
		array(
			'default'           => '#FFF',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_content_bg_color',
			array(
				'label'    => __( 'Content Background Color', 'fituet' ),
				'section'  => 'fituet_element_options',
				'settings' => 'fituet_content_bg_color',
				'priority' => 7
			)
		)
	);

	$wp_customize->add_section(
		'fituet_typography_options', array(
			'title'       => __( 'Fituet Typography Color', 'fituet' ),
			'description' => sprintf( __( 'Use the following settings change color for typography such as links, headings and content', 'fituet' ) ),
			'priority'    => 33,
		)
	);

	$wp_customize->add_setting(
		'fituet_entry_color',
		array(
			'default'           => '#444',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_entry_color',
			array(
				'label'    => __( 'Entry Content Color', 'fituet' ),
				'section'  => 'fituet_typography_options',
				'settings' => 'fituet_entry_color'
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_header_color',
		array(
			'default'           => '#444',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_header_color',
			array(
				'label'    => __( 'Header/Title Color', 'fituet' ),
				'section'  => 'fituet_typography_options',
				'settings' => 'fituet_header_color',
				'priority' => 8
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_link_color',
		array(
			'default'           => '#015198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_link_color',
			array(
				'label'    => __( 'Link Color', 'fituet' ),
				'section'  => 'fituet_typography_options',
				'settings' => 'fituet_link_color',
				'priority' => 11
			)
		)
	);

	$wp_customize->add_setting(
		'fituet_link_hover_color',
		array(
			'default'           => '#016198',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'fituet_link_hover_color',
			array(
				'label'    => __( 'Link Hover Color', 'fituet' ),
				'section'  => 'fituet_typography_options',
				'settings' => 'fituet_link_hover_color',
				'priority' => 12
			)
		)
	);

	$wp_customize->add_section(
		'fituet_footer_options', array(
			'title'       => __( 'Fituet Footer', 'fituet' ),
			'description' => sprintf( __( 'Use the following settings customize Footer', 'fituet' ) ),
			'priority'    => 34,
		)
	);

	$wp_customize->add_setting(
		'fituet_footer_textbox',
		array(
			'default'           => 'Default footer text',
			'sanitize_callback' => 'fituet_sanitize_hexcolor',
		)
	);

	$wp_customize->add_control(
		'fituet_footer_textbox',
		array(
			'label'   => __( 'Copyright text', 'fituet' ),
			'section' => 'fituet_footer_options',
			'type'    => 'text',
		)
	);

	$wp_customize->get_setting( 'fituet_menu_color' )->transport       = 'postMessage';
	$wp_customize->get_setting( 'fituet_menu_hover_color' )->transport = 'postMessage';
	$wp_customize->get_setting( 'fituet_entry_color' )->transport      = 'postMessage';
	$wp_customize->get_setting( 'fituet_element_color' )->transport    = 'postMessage';
	$wp_customize->get_setting( 'fituet_logo_color' )->transport       = 'postMessage';
	$wp_customize->get_setting( 'fituet_header_color' )->transport     = 'postMessage';
	$wp_customize->get_setting( 'fituet_wrapper_color' )->transport    = 'postMessage';
	$wp_customize->get_setting( 'fituet_content_bg_color' )->transport = 'postMessage';
	$wp_customize->get_setting( 'fituet_menu_item_color' )->transport  = 'postMessage';

}

add_action( 'customize_register', 'fituet_options_register_theme_customizer' );

/**
 * Adds sanitization callback function: colors
 * @package Fituet
 */
function fituet_sanitize_hexcolor( $color ) {
	if ( $unhashed = sanitize_hex_color_no_hash( $color ) ) {
		return '#' . $unhashed;
	}

	return $color;
}

/**
 * Adds sanitization callback function: text
 * @package Fituet
 */
function fituet_sanitize_text( $input ) {
	return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Output custom CSS in theme header
 * @package Fituet
 */
function fituet_customizer_css() {
	?>
	<!--<style type="text/css">
		a {
			color: <?php echo get_theme_mod( 'fituet_link_color', '#015198' ); ?>;
		}

		#site-title a {
			color: <?php echo get_theme_mod( 'fituet_logo_color' ); ?>;
		}

		#site-title a:hover {
			color: <?php echo get_theme_mod( 'fituet_logo_hover_color' ); ?>;
		}

		.wrapper {
			background: <?php echo get_theme_mod( 'fituet_wrapper_color', '#F8F8F8' ); ?>;
		}

		.social-icons ul li a {
			color: <?php echo get_theme_mod( 'fituet_social_color', '#d0d0d0' ); ?>;
		}

		#main-nav a, #main-nav a:hover, #main-nav ul li.current-menu-item a, #main-nav ul li.current_page_ancestor a, #main-nav ul li.current-menu-ancestor a, #main-nav ul li.current_page_item a, #main-nav ul li:hover > a {
			color: <?php echo get_theme_mod( 'fituet_menu_item_color', '#fff' ); ?>;
		}

		.widget, article {
			background: <?php echo get_theme_mod( 'fituet_content_bg_color', '#fff' ); ?>;
		}

		.entry-title, .entry-title a, h1, h2, h3, h4, h5, h6, .widget-title {
			color: <?php echo get_theme_mod( 'fituet_header_color', '#1b1e1f' ); ?>;
		}

		a:focus, a:active, a:hover, .tags a:hover, .custom-gallery-title a, .widget-title a, #content ul a:hover, #content ol a:hover, .widget ul li a:hover, .entry-title a:hover, .entry-meta a:hover, #site-generator .copyright a:hover {
			color: <?php echo get_theme_mod( 'fituet_link_hover_color', '#016198' ); ?>;
		}

		#main-nav {
			background: <?php echo get_theme_mod( 'fituet_menu_color', '#015198' ); ?>;
			border-color: <?php echo get_theme_mod( 'fituet_menu_color', '#015198' ); ?>;
		}

		#main-nav ul li ul, body {
			border-color: <?php echo get_theme_mod( 'fituet_menu_color', '#016198' ); ?>;
		}

		#main-nav a:hover, #main-nav ul li.current-menu-item a, #main-nav ul li.current_page_ancestor a, #main-nav ul li.current-menu-ancestor a, #main-nav ul li.current_page_item a, #main-nav ul li:hover > a, #main-nav li:hover > a, #main-nav ul ul :hover > a, #main-nav a:focus {
			background: <?php echo get_theme_mod( 'fituet_menu_hover_color', '#016198' ); ?>;
		}

		#main-nav ul li ul li a:hover, #main-nav ul li ul li:hover > a, #main-nav ul li.current-menu-item ul li a:hover {
			color: <?php echo get_theme_mod( 'fituet_menu_hover_color', '#016198' ); ?>;
		}

		.entry-content {
			color: <?php echo get_theme_mod( 'fituet_entry_color', '#1D1D1D' ); ?>;
		}

		input[type="reset"], input[type="button"], input[type="submit"], .entry-meta-bar .readmore, #controllers a:hover, #controllers a.active, .pagination span, .pagination a:hover span, .wp-pagenavi .current, .wp-pagenavi a:hover {
			background: <?php echo get_theme_mod( 'fituet_element_color', '#015198' ); ?>;
			border-color: <?php echo get_theme_mod( 'fituet_element_color', '#015198' ); ?> !important;
		}

		::selection {
			background: <?php echo get_theme_mod( 'fituet_element_color', '#015198' ); ?>;
		}

		blockquote {
			border-color: <?php echo get_theme_mod( 'fituet_element_color', '#016198' ); ?>;
		}

		#controllers a:hover, #controllers a.active {
			color: <?php echo get_theme_mod( 'fituet_element_color', ' #016198' ); ?>;
		}

		input[type="reset"]:hover, input[type="button"]:hover, input[type="submit"]:hover, input[type="reset"]:active, input[type="button"]:active, input[type="submit"]:active, .entry-meta-bar .readmore:hover, .entry-meta-bar .readmore:active, ul.default-wp-page li a:hover, ul.default-wp-page li a:active {
			background: <?php echo get_theme_mod( 'fituet_element_hover_color', '#016198' ); ?>;
			border-color: <?php echo get_theme_mod( 'fituet_element_hover_color', '#016198' ); ?>;
		}
	</style>-->
	<?php
}

add_action( 'wp_head', 'fituet_customizer_css' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * @package Fituet
 */
function fituet_customize_preview_js() {
	wp_enqueue_script( 'fituet_customizer', get_template_directory_uri() . '/library/js/customizer.js', array( 'customize-preview' ), '20140425', true );
}

add_action( 'customize_preview_init', 'fituet_customize_preview_js' );

?>