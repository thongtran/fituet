<?php
/**
 * Displays the index section of the theme.
 *
 */
?>

<?php get_header(); ?>

<?php
/**
 * fituet_before_main_container hook
 */
do_action( 'fituet_before_main_container' );
?>

<div id="container">
	<?php
	/**
	 * fituet_main_container hook
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * fituet_content 10
	 */
	do_action( 'fituet_main_container' );
	?>
</div><!-- #container -->

<?php
/**
 * fituet_after_main_container hook
 */
do_action( 'fituet_after_main_container' );
?>

<?php get_footer(); ?>
