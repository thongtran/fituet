<?php
/**
 * Displays the searchform of the theme.
 */
?>
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s__" placeholder="<?php esc_attr_e( 'Search', 'fituet' ); ?>">
	</div>
</form>