<?php
/**
 * This file displays page with no sidebar.
 *
 */
?>


<?php
/**
 * fituet_before_loop_content
 *
 * HOOKED_FUNCTION_NAME PRIORITY
 *
 * fituet_loop_before 10
 */
do_action( 'fituet_before_loop_content' );

/**
 * fituet_loop_content
 *
 * HOOKED_FUNCTION_NAME PRIORITY
 *
 * fituet_theloop 10
 */
do_action( 'fituet_loop_content' );

/**
 * fituet_after_loop_content
 *
 * HOOKED_FUNCTION_NAME PRIORITY
 *
 * fituet_next_previous 5
 * fituet_loop_after 10
 */
do_action( 'fituet_after_loop_content' );
?>