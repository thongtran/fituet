<?php if ( bp_has_members( 'type=alphabetical&member_type=fituet_teacher' ) ) : ?>

	<!--	<div id="pag-top" class="pagination">-->
	<!---->
	<!--		<div class="pag-count" id="member-dir-count-top">-->
	<!---->
	<!--			--><?php //bp_members_pagination_count(); ?>
	<!---->
	<!--		</div>-->
	<!---->
	<!--		<div class="pagination-links" id="member-dir-pag-top">-->
	<!---->
	<!--			--><?php //bp_members_pagination_links(); ?>
	<!---->
	<!--		</div>-->
	<!---->
	<!--	</div>-->

	<?php do_action( 'bp_before_directory_members_list' ); ?>

	<table>
		<thead>
		<tr>
			<th><?php _e( 'Name', 'fituet' ); ?></th>
			<th><?php _e( 'Degree', 'fituet' ); ?></th>
			<th><?php _e( 'Department', 'fituet' ); ?></th>
			<th><?php _e( 'Teaching', 'fituet' ); ?></th>
			<th><?php _e( 'Research', 'fituet' ); ?></th>
		</tr>
		</thead>
		<tbody>

		<?php while ( bp_members() ) : bp_the_member(); ?>
			<tr>
				<td><a href="<?php bp_member_permalink(); ?>"><?php bp_member_name(); ?></a></td>
				<td><?php echo get_user_meta( bp_get_member_user_id(), 'fituet_degree', true ); ?></td>
				<td><?php echo esc_html( get_the_title( get_user_meta( bp_get_member_user_id(), 'fituet_department', true ) ) ); ?></td>
				<td><?php echo nl2br( get_user_meta( bp_get_member_user_id(), 'fituet_teaching', true ) ); ?></td>
				<td><?php echo nl2br( get_user_meta( bp_get_member_user_id(), 'fituet_research', true ) ); ?></td>
			</tr>
		<?php endwhile; ?>
		</tbody>
	</table>

	<?php do_action( 'bp_after_directory_members_list' ); ?>

	<!--	--><?php //bp_member_hidden_fields(); ?>

	<div id="pag-bottom" class="pagination">

		<div class="pag-count" id="member-dir-count-bottom">

			<?php bp_members_pagination_count(); ?>

		</div>

		<div class="pagination-links" id="member-dir-pag-bottom">

			<?php bp_members_pagination_links(); ?>

		</div>

	</div>

<?php else: ?>

	<div id="message" class="info">
		<p><?php _e( "Sorry, no members were found.", 'fituet' ); ?></p>
	</div>

<?php endif; ?>