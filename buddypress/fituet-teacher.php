<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 31/8/2015
 * Time: 7:19 PM
 */

/**
 * Register Profile Teacher Script
 */
function fituet_enqueue_profile_teacher_script() {
	wp_register_script(
		'teacher-profile-script', get_template_directory_uri() . '/buddypress/assets/js/profile.js', array( 'jquery' )
	);
	wp_enqueue_script( 'teacher-profile-script' );
}

add_action( 'fituet_xprofile_script', 'fituet_enqueue_profile_teacher_script' );

/**
 * Register member types
 */
function fituet_register_member_types_teacher() {
	bp_register_member_type(
		'fituet_teacher', array(
			'labels' => array(
				'name'          => __( 'Teachers', 'fituet' ),
				'singular_name' => __( 'Teacher', 'fituet' ),
			),
		)
	);
}

add_action( 'bp_init', 'fituet_register_member_types_teacher' );

/**
 * Check user format "Teacher"
 */
function fituet_is_teacher( $user_id ) {
	if ( bp_get_member_type( $user_id ) == 'fituet_teacher' ) {
		return true;
	}

	return false;
}

function fituet_get_departments() {
	$args = array(
		'post_type'      => 'fituet_department',
		'posts_per_page' => - 1,
	);

	$the_query = new WP_Query( $args );

	$arr = array();

	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();

			$id    = get_the_ID();
			$title = get_the_title();

			$arr[] = array(
				'id'    => $id,
				'title' => $title
			);
		}
	}
	wp_reset_postdata();

	return $arr;
}

/**
 * Add form extra UET profile teacher
 *
 * @param $user
 */
function fituet_extra_profile_teacher( $user ) {
	if ( ! fituet_is_teacher( $user->ID ) ) {
		return;
	}

	do_action( 'fituet_xprofile_script' );
	?>

	<h3><?php esc_html_e( 'UET Profile Information', 'fituet' ); ?></h3>

	<table class="form-table">
		<!-- Degree -->
		<tr>
			<th>
				<label for="fituet_degree"><?php esc_html_e( 'Degree', 'fituet' ); ?></label>
			</th>
			<td>
				<input type="text" name="fituet_degree" id="fituet_degree"
					   value="<?php echo esc_attr( get_the_author_meta( 'fituet_degree', $user->ID ) ); ?>"
					   class="regular-text" />
			</td>
		</tr>

		<!-- Department -->
		<tr>
			<th>
				<label for="fituet_department"><?php esc_html_e( 'Department', 'fituet' ); ?></label>
			</th>
			<td>
				<select name="fituet_department" id="fituet_department" class="regular-text">
					<?php
					$departments  = fituet_get_departments();
					$u_department = get_user_meta( $user->ID, 'fituet_department', true );

					foreach ( $departments as $index => $department ) {
						if ( $u_department == $department['id'] ) {
							echo '<option value="' . $department['id'] . '" selected="selected">' . $department['title'] . '</option>';
							continue;
						}
						echo '<option value="' . $department['id'] . '">' . $department['title'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>

		<!-- Research orientation -->
		<tr>
			<th>
				<label for="fituet_research"><?php esc_html_e( 'Research orientation', 'fituet' ); ?></label>
			</th>
			<td>
				<textarea name="fituet_research" id="fituet_research" rows="10"
						  cols="30"><?php echo esc_attr( get_the_author_meta( 'fituet_research', $user->ID ) ); ?></textarea>
			</td>
		</tr>

		<!-- Teaching -->
		<tr>
			<th>
				<label for="fituet_teaching"><?php esc_html_e( 'Teaching', 'fituet' ); ?></label>
			</th>
			<td>
				<textarea name="fituet_teaching" id="fituet_teaching" rows="10"
						  cols="30"><?php echo esc_attr( get_the_author_meta( 'fituet_teaching', $user->ID ) ); ?></textarea>
			</td>
		</tr>

		<!-- Other -->
		<tr>
			<th>
				<label for="fituet_teacher_other"><?php esc_html_e( 'Other', 'fituet' ); ?></label>
			</th>
			<td>
				<textarea name="fituet_teacher_other" id="fituet_teacher_other" rows="5"
						  cols="30"><?php echo esc_attr( get_the_author_meta( 'fituet_teacher_other', $user->ID ) ); ?></textarea>
			</td>
		</tr>
	</table>
<?php }

/**
 * Save extra UET profile teacher
 *
 * @param $user_id
 */
function fituet_save_extra_profile_teacher( $user_id ) {
	if ( ! current_user_can( 'edit_user', $user_id ) ) {
		return;
	}

	update_user_meta( $user_id, 'fituet_degree', $_POST['fituet_degree'] );
	//	update_user_meta( $user_id, 'fituet_worklocation', $_POST['fituet_worklocation'] );
	update_user_meta( $user_id, 'fituet_department', $_POST['fituet_department'] );
	update_user_meta( $user_id, 'fituet_research', $_POST['fituet_research'] );
	update_user_meta( $user_id, 'fituet_teaching', $_POST['fituet_teaching'] );
	update_user_meta( $user_id, 'fituet_teacher_other', $_POST['fituet_teacher_other'] );
}

add_action( 'show_user_profile', 'fituet_extra_profile_teacher' );
add_action( 'edit_user_profile', 'fituet_extra_profile_teacher' );

add_action( 'personal_options_update', 'fituet_save_extra_profile_teacher' );
add_action( 'edit_user_profile_update', 'fituet_save_extra_profile_teacher' );