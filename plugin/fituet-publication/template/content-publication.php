<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 10/10/2015
 * Time: 10:30 PM
 */

/**
 * Function to show the publication content.
 */
if ( ! function_exists( 'fituet_theloop_for_publication_type' ) ) {
	function fituet_theloop_for_publication_type() {
		global $post; ?>

		<section id="publication-<?php the_ID(); ?>" <?php post_class(); ?>>
			<article>
				<header class="entry-header">
					<h2 class="entry-title">
						<?php the_title(); ?>
					</h2>
				</header>

				<section class="entry-body">
					<div class="entry-content clearfix">
						<?php fituet_show_table_content_publication( $post->ID ); ?>
					</div>
				</section>

			</article>
		</section>
		<?php
	}
}

/**
 * Table publication content
 *
 * @param $post_id
 */
function fituet_show_table_content_publication( $post_id ) {
	?>
	<table>
		<tbody>
		<tr>
			<td><?php esc_html_e( 'Title', 'fituet' ); ?></td>
			<td><?php echo esc_html( get_the_title() ); ?></td>
		</tr>
		<tr>
			<td><?php esc_html_e( 'Publication type', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_type( $post_id ) ); ?></td>
		</tr>

		<!-- Year of Publication -->
		<?php if ( fituet_get_publication_year( $post_id ) != '' ): ?>
			<tr>
				<td><?php esc_html_e( 'Year of Publication', 'fituet' ); ?></td>
				<td><?php echo esc_html( fituet_get_publication_year( $post_id ) ); ?></td>
			</tr>
		<?php endif; ?>
		<!-- End Year of Publication -->

		<!-- Authors of Publication -->
		<?php if ( fituet_get_publication_authors_html( $post_id ) != '' ): ?>
			<tr>
				<td><?php esc_html_e( 'Authors', 'fituet' ); ?></td>
				<td><?php echo fituet_get_publication_authors_html( $post_id ); ?></td>
			</tr>
		<?php endif; ?>
		<!-- End Authors of Publication -->

		<?php if ( fituet_get_publication_type_val( $post_id ) == 'article' ) {
			fituet_the_extra_content_publication_article( $post_id );
		} ?>

		<?php if ( fituet_get_publication_type_val( $post_id ) == 'inproceedings' ) {
			fituet_the_extra_content_publication_inproceedings( $post_id );
		} ?>

		<?php if ( fituet_get_publication_type_val( $post_id ) == 'book' ) {
			fituet_the_extra_content_publication_book( $post_id );
		} ?>

		<?php if ( fituet_get_publication_type_val( $post_id ) == 'misc' ) {
			fituet_the_extra_content_publication_misc( $post_id );
		} ?>
		</tbody>
	</table>

	<?php
}

/**
 * Show extra content of article publication
 *
 * @param $post_id
 */
function fituet_the_extra_content_publication_article( $post_id ) {
	?>

	<!-- Journal -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'article_journal' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Journal', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'article_journal' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Journal -->

	<!-- Volume -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'article_volume' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Volume', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'article_volume' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Volume -->

	<!-- Issue -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'article_issue' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Issue', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'article_issue' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Issue -->

	<!-- Pages -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'article_pages' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Pages', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'article_pages' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Pages -->

	<?php
}

/**
 * Show extra content of inproceedings publication
 *
 * @param $post_id
 */
function fituet_the_extra_content_publication_inproceedings( $post_id ) {
	?>

	<!-- Book title -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'inproceedings_booktitle' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Book title', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'inproceedings_booktitle' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- Book title -->

	<!-- Publisher -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'inproceedings_publisher' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Publisher', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'inproceedings_publisher' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Publisher -->

	<!-- Address -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'inproceedings_address' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Address', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'inproceedings_address' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Address -->

	<!-- Pages -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'inproceedings_pages' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Pages', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'inproceedings_pages' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Pages -->

	<?php
}

/**
 * Show extra content of book publication
 *
 * @param $post_id
 */
function fituet_the_extra_content_publication_book( $post_id ) {
	?>

	<!-- Publisher -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'book_publisher' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Publisher', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'book_publisher' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Publisher -->

	<!-- Address -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'book_address' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Address', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'book_address' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Address -->


	<?php
}

/**
 * Show extra content of misc publication
 *
 * @param $post_id
 */
function fituet_the_extra_content_publication_misc( $post_id ) {
	?>

	<!-- How published -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'misc_howpublished' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'How published', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'misc_howpublished' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End How published -->

	<!-- Month -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'misc_month' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Month', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'misc_month' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Month -->

	<!-- Key -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'misc_key' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Key', 'fituet' ); ?></td>
			<td><?php echo esc_html( fituet_get_publication_custom_field( $post_id, 'misc_key' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Key -->

	<!-- Note -->
	<?php if ( fituet_get_publication_custom_field( $post_id, 'misc_note' ) != '' ): ?>
		<tr>
			<td><?php esc_html_e( 'Note', 'fituet' ); ?></td>
			<td><?php echo nl2br( fituet_get_publication_custom_field( $post_id, 'misc_note' ) ); ?></td>
		</tr>
	<?php endif; ?>
	<!-- End Note -->

	<?php
}