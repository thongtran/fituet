<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 10/10/2015
 * Time: 8:44 PM
 */

require_once 'content-publication.php';

get_header();
?>

	<div id="container">
		<?php
		if ( function_exists( 'fituet_loop_before' ) ) {
			fituet_loop_before();
		}

		/**
		 * Publication content
		 */
		fituet_theloop_for_publication_type();

		if ( function_exists( 'fituet_loop_after' ) ) {
			fituet_loop_after();
		}
		?>
	</div><!-- #container -->

<?php get_footer();