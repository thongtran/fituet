<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 27/8/2015
 * Time: 10:59 PM
 */

if ( ! function_exists( 'fituet_add_filter_link_slider_begin' ) ) {
	function fituet_add_filter_link_slider_begin( $current, $post_id ) {
		$link = fituet_get_link_slider( $post_id );

		if ( isset( $link ) && $link != '' ) {
			$post_title = get_the_title( $post_id );

			return '<a href="' . $link . '" title="' . $post_title . '">';
		}

		return '';
	}
}
add_filter( 'fituet_link_slider_filter_begin', 'fituet_add_filter_link_slider_begin', 1, 2 );

if ( ! function_exists( 'fituet_add_filter_link_slider_end' ) ) {
	function fituet_add_filter_link_slider_end( $current, $post_id ) {
		$link = fituet_get_link_slider( $post_id );

		if ( isset( $link ) && $link != '' ) {
			return '</a>';
		}

		return '';
	}
}
add_filter( 'fituet_link_slider_filter_end', 'fituet_add_filter_link_slider_end', 1, 2 );

function fituet_get_link_slider( $post_id ) {
	$link = get_post_meta( $post_id, 'fituet_link_slider_meta_box_nonce', true );

	if ( isset( $link ) && $link != '' ) {
		return $link;
	}

	return '';
}