<?php
/**
 * Created by PhpStorm.
 * User: Tu TV
 * Date: 13/10/2015
 * Time: 4:11 PM
 */

/**
 * Adds Fituet_Partner_Horizontal_Widget widget.
 */
class Fituet_Partner_Horizontal_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Fituet_Partner_Horizontal_Widget', // Base ID
			__( 'FIT Partners Carousel', 'fituet' ), // Name
			array(
				'description' => __( 'FIT Partners Carousel (horizontal).', 'fituet' ),
			) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters(
					'widget_title',
					$instance['title']
				) . $args['after_title'];
		}

		if ( ! empty( $instance['per_page'] ) ) {
			$show = $instance['per_page'];
		} else {
			$show = 5;
		}
		fituet_loop_show_partner_horizontal( $show );
		fituet_loop_show_partner( $show );

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title    = ! empty( $instance['title'] ) ? $instance['title']
			: __( '', 'fituet' );
		$per_page = ! empty( $instance['per_page'] ) ? $instance['per_page']
			: 5;
		?>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat"
				   id="<?php echo $this->get_field_id( 'title' ); ?>"
				   name="<?php echo $this->get_field_name( 'title' ); ?>"
				   type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'per_page' ); ?>"><?php _e( 'Number of partners:' ); ?></label>
			<input class="widefat"
				   id="<?php echo $this->get_field_id( 'per_page' ); ?>"
				   name="<?php echo $this->get_field_name( 'per_page' ); ?>"
				   type="number" value="<?php echo esc_attr( $per_page ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance             = array();
		$instance['title']    = ( ! empty( $new_instance['title'] ) )
			? strip_tags( $new_instance['title'] ) : '';
		$instance['per_page'] = ( ! empty( $new_instance['per_page'] ) )
			? strip_tags( $new_instance['per_page'] ) : '';

		return $instance;
	}

} // class Fituet_Partner_Horizontal_Widget

// register Fituet_Partner_Horizontal_Widget widget
function register_Fituet_Partner_Horizontal_Widget() {
	register_widget( 'Fituet_Partner_Horizontal_Widget' );
}

add_action( 'widgets_init', 'register_Fituet_Partner_Horizontal_Widget' );