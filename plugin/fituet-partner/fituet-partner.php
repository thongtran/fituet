<?php
/**
 * Plugin Name: FIT Partners
 * Plugin URI: http://thimpress.com
 * Description: FIT Partners Manager
 * Version: 1.0
 * Author: Tu TV
 * Author URI: http://tutran.me
 * License: GNU General Public License v2 or later
 */

define( 'FIT_PARTNER_DIR', plugin_dir_path( __FILE__ ) );
define( 'FIT_PARTNER_URL', plugin_dir_url( __FILE__ ) );

function fituet_register_partner_post_type() {

	$labels  = array(
		'name'               => _x(
			'Partners', 'Post Type General Name',
			'fituet'
		),
		'singular_name'      => _x(
			'Partner', 'Post Type Singular Name',
			'fituet'
		),
		'menu_name'          => __( 'Partner', 'fituet' ),
		'name_admin_bar'     => __( 'Partner', 'fituet' ),
		'parent_item_colon'  => __( 'Parent Partner:', 'fituet' ),
		'all_items'          => __( 'All Partners', 'fituet' ),
		'add_new_item'       => __( 'Add New Partner', 'fituet' ),
		'add_new'            => __( 'Add New', 'fituet' ),
		'new_item'           => __( 'New Partner', 'fituet' ),
		'edit_item'          => __( 'Edit Partner', 'fituet' ),
		'update_item'        => __( 'Update Partner', 'fituet' ),
		'view_item'          => __( 'View Partner', 'fituet' ),
		'search_items'       => __( 'Search Partner', 'fituet' ),
		'not_found'          => __( 'Not found', 'fituet' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'fituet' ),
	);
	$rewrite = array(
		'slug'       => 'partner',
		'with_front' => true,
		'pages'      => true,
		'feeds'      => true,
	);
	$args    = array(
		'label'               => __( 'Partner', 'fituet' ),
		'description'         => __( 'FIT Partners', 'fituet' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 7,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
		'menu_icon'           => 'dashicons-smiley',
	);
	register_post_type( 'fituet_partner', $args );

}

add_action( 'init', 'fituet_register_partner_post_type', 0 );

function fituet_loop_show_partner( $per_page ) {
	$args = array(
		'post_type'      => array( 'fituet_partner' ),
		'posts_per_page' => - 1,
		'orderby'        => 'rand',
	);

	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) {
		echo '<div class="fituet_partners vticker">';
		echo '<ul class="list-partners" data-number="' . $per_page . '">';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			echo '<li class="partner">';
			echo '<a href="' . get_permalink() . '" title="' . get_the_title()
				. '">';
			echo get_the_post_thumbnail();
			echo '</a></li>';
		}
		echo '</ul></div>';
	}
	/* Restore original Post Data */
	wp_reset_postdata();

	/**
	 * Register script
	 */
	wp_enqueue_script(
		'jquery-vticker',
		FIT_PARTNER_URL . 'assets/js/jquery.vticker.min.js',
		array( 'jquery' )
	);
	wp_enqueue_script(
		'fituet-widget-partner',
		FIT_PARTNER_URL . 'assets/js/widget-partner.js',
		array( 'jquery', 'jquery-vticker' )
	);
}

function fituet_loop_show_partner_horizontal( $per_page ) {
	$args = array(
		'post_type'      => array( 'fituet_partner' ),
		'posts_per_page' => - 1,
		'orderby'        => 'rand',
	);

	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) {
		echo '<div class="fituet_partners_horizontal">';
		echo '<ul class="list-partners" data-number="' . $per_page . '">';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			echo '<li class="partner item">';
			echo '<a href="' . get_permalink() . '" title="' . get_the_title()
				. '">';
			echo get_the_post_thumbnail( get_the_ID(), 'small' );
			echo '</a></li>';
		}
		echo '</ul></div>';
	}
	/* Restore original Post Data */
	wp_reset_postdata();

	/**
	 * Register script
	 */
	wp_enqueue_script(
		'jquery-owlcarousel',
		FIT_PARTNER_URL . 'assets/js/owl.carousel.min.js',
		array( 'jquery' )
	);
	wp_enqueue_script(
		'fituet-widget-partner',
		FIT_PARTNER_URL . 'assets/js/widget-partner_horizontal.js',
		array( 'jquery', 'jquery-owlcarousel' )
	);

	wp_enqueue_style( 'owl-theme', FIT_PARTNER_URL . 'assets/css/owl.carousel.css' );
}

/**
 * Register widget
 */
require_once 'partner-widget.php';
require_once 'partner-widget-horizontal.php';